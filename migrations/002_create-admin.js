const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
dotenv.config();

const password = process.env.FIRST_ADMIN_PASSWORD;
const salt = process.env.FIRST_ADMIN_SALT;
const passwordHash = bcrypt.hashSync(password, salt);

module.exports.generateSql = () => `
  INSERT INTO users
  ("name", "email", "salt", "passwordHash")
  VALUES('admin', 'admin@chat.com', '${salt}', '${passwordHash}');
`;
