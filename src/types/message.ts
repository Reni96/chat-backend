export type Message = {
  id?: number;
  roomId: number;
  data: string;
  createdAt: string | Date;
  userId: number;
};
