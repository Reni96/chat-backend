export type UserRegister = {
  id: number;
  name: string;
  email: string;
};
