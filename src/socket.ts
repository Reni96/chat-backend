/* eslint-disable @typescript-eslint/explicit-function-return-type */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import { Server } from 'socket.io';

export const createSocket = (io: Server) => {
  return {
    sendMessage: (message: string): void => {
      io.emit('chat', message);
    },
  };
};
