export const UserSchema = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    name: { type: 'string' },
    email: { type: 'string' },
    salt: { type: 'string' },
    passwordHash: { type: 'string' },
  },
};
