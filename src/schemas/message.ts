export const MessageSchema = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    roomId: { type: 'number' },
    data: { type: 'string' },
    createdAt: { type: 'string' },
    userId: { type: 'number' },
  },
};

export const MessageArraySchema = {
  type: 'array',
  items: MessageSchema,
};
