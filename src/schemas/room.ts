export const RoomSchema = {
  type: 'object',
  properties: {
    id: { type: 'number' },
    name: { type: 'string' },
    password: { type: 'string' },
  },
};
