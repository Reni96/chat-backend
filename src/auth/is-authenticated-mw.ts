import passport from 'fastify-passport';

export const isAuthenticated = passport.authenticate('local', async (req, res) => {
  if (req.user) {
    return Promise.resolve();
  }
  return res.status(401).send({ error: 'Unauthorized' });
});
