import { Pool } from 'pg';

export const createDb = async (connectionString: string): Promise<Pool> => {
  return new Pool({ connectionString });
};
