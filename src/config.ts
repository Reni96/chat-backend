import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config();
dotenv.config({
  path: `.env${process.env.NODE_ENV ? `.${process.env.NODE_ENV}` : ''}`,
});

const convictConfig = convict({
  env: {
    doc: 'environment',
    format: ['prod', 'dev', 'test'],
    default: 'dev',
    env: 'NODE_ENV',
  },
  frontendUrl: {
    doc: 'frontend_url',
    format: String,
    default: 'http://localhost:4200',
    env: 'FRONTEND_URL',
  },
  port: {
    doc: 'port',
    format: String,
    default: '3000',
    env: 'PORT',
  },
  databaseUrl: {
    doc: 'URL of the Database',
    format: String,
    default: 'postgresql://user:password@localhost:5432/db',
    env: 'DATABASE_URL',
  },
  cookieKey: {
    doc: 'Cookie key',
    format: String,
    default: 'c6c1efee2bb358789c4b2b9b3e43abb3e4b340c812b8bc584c611823918a426b',
    env: 'COOKIE_KEY',
  },
  databaseName: {
    doc: 'Name of the Database',
    format: String,
    default: 'chatdb',
    env: 'DATABASE_NAME',
  },
});

convictConfig.validate({ allowed: 'strict' });
export const config = convictConfig.getProperties();
export type ConfigObject = typeof config;
