/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import bcrypt from 'bcrypt';
import fastify from 'fastify';
import fastifyCors from 'fastify-cors';
import passport from 'fastify-passport';
import fastifySecureSession from 'fastify-secure-session';
import fastifySocketIO from 'fastify-socket.io';
import passportLocal from 'passport-local';
import path from 'path';
import { createDb as cdb, migrate } from 'postgres-migrations';
import { Server } from 'socket.io';
import { config } from './config';
import { createDb } from './database';
import { createRoomEndpointFactory } from './endpoints/create-room.endpoint';
import { getMessagesEndpointFactory } from './endpoints/get-messages.endpoint';
import { getRoomEndpointFactory } from './endpoints/get-room.enpoint';
import { getRoomsEndpointFactory } from './endpoints/get-rooms.endpoint';
import { getStatusEnpointFactory } from './endpoints/get-status.endpoint';
import { getUsersEndpointFactory } from './endpoints/get-users.endpoint';
import { loginEndpointFactory } from './endpoints/login.endpoint';
import { registerEndpointFactory } from './endpoints/register.endpoint';
import { sendMessageEndpointFactory } from './endpoints/send-message.endpoint';
import { createMessagesRepo, MessagesRepo } from './repos/messages.repo';
import { createRoomsRepo, RoomsRepo } from './repos/rooms.repo';
import { createUsersRepo, UsersRepo } from './repos/users.repo';
import { User } from './types/user';

const ONE_DAY = 86400000;

const main = async (): Promise<void> => {
  const server = fastify();

  server.setErrorHandler((error, _, reply) => {
    console.log(error);
    if (error.validation) {
      void reply.status(422).send({
        ...error.validation,
      });
      return;
    }
    void reply.status(500).send({
      errorCode: 'INTERNAL_SERVER_ERROR',
    });
  });

  //adatbázis kapcsolat
  const connectionString = config.databaseUrl;
  const pool = await createDb(connectionString);

  const client = await pool.connect();
  try {
    await cdb(config.databaseName, { client });
    await migrate({ client }, path.join(__dirname, '..', 'migrations'));
  } catch (e) {
    console.error(e);
    process.exit(1);
  } finally {
    client.release();
  }

  void server.register(fastifyCors, {
    origin: config.frontendUrl,
  });
  // void server.register(fastifySocketIO, { cors: { origin: '*' } });
  void server.register(fastifySocketIO, { cors: { origin: config.frontendUrl } });

  const roomsRepo: RoomsRepo = createRoomsRepo(pool);
  const messagesRepo: MessagesRepo = createMessagesRepo(pool);
  const usersRepo: UsersRepo = createUsersRepo(pool);

  passport.use(
    'local',
    new passportLocal.Strategy(
      {
        usernameField: 'email',
        passwordField: 'password',
      },
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      async (email, password, cb) => {
        const user = await usersRepo.getUserByEmail(email);
        if (!user) {
          return cb(null, false, { message: 'Insufficient permission' });
        }
        const { salt, passwordHash } = user;
        const comparePassword = await bcrypt.hash(password, salt).catch(() => null);
        if (passwordHash !== comparePassword) {
          return cb(null, false, { message: 'Insufficient permission' });
        }
        return cb(null, user);
      },
    ),
  );

  void passport.registerUserSerializer(async (user: User) => user.id);
  void passport.registerUserDeserializer(async id => usersRepo.getUserById(id as number));

  void server.register(fastifySecureSession, {
    key: Buffer.from(config.cookieKey, 'hex'),
    cookie: { path: '/', maxAge: ONE_DAY },
    cookieName: 'chatCookie',
  });

  void server.register(passport.initialize()); // Used to initialize passport
  void server.register(passport.secureSession()); // Used to persist login session

  createRoomEndpointFactory(server, roomsRepo);
  getRoomEndpointFactory(server, roomsRepo);
  getRoomsEndpointFactory(server, roomsRepo);
  sendMessageEndpointFactory(server, messagesRepo);
  getMessagesEndpointFactory(server, messagesRepo);
  getStatusEnpointFactory(server);
  loginEndpointFactory(server, usersRepo);
  registerEndpointFactory(server, usersRepo);
  getUsersEndpointFactory(server, usersRepo);

  void server.ready().then(() => {
    console.log('app started');
    const io = server.io as Server;

    io.on('connect', socket => {
      console.log((socket.client as any).id, 'connected');

      socket.on('disconnect', () => {
        console.log((socket.client as any).id, 'disconnected');
      });
    });
  });

  server.listen(config.port, (err, address) => {
    if (err) {
      console.error(err);
      process.exit(1);
    }
    console.log(`Server listening at ${address}`);
  });
};

void main();
