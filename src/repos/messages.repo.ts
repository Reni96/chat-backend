import { Pool, QueryResult } from 'pg';
import { Message } from '../types/message';

export type MessagesRepo = {
  listByRoomId: (roomId: number) => Promise<QueryResult<Message[]>>;
  add: (roomId: number, message: Message) => Promise<QueryResult<Message>>;
};

export const createMessagesRepo = (pool: Pool): MessagesRepo => {
  return {
    listByRoomId: async roomId => pool.query('SELECT * FROM messages WHERE "roomId"=$1', [roomId]),
    add: async (roomId: number, message: Message) => {
      console.log(message);
      const result = await pool.query(
        'INSERT INTO messages("roomId", "data", "createdAt", "userId") VALUES ($1, $2, $3, $4) RETURNING *;',
        [roomId, message.data, message.createdAt, message.userId],
      );
      console.log(result.rows[0]);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return result.rows[0];
    },
  };
};
