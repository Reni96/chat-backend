import { Pool, QueryResult } from 'pg';
import { Room } from '../types/room';

export type RoomsRepo = {
  list: () => Promise<QueryResult<Room[]>>;
  get: (roomId: number) => Promise<QueryResult<Room>>;
  add: (name: string, passwordHash: string, salt: string) => Promise<QueryResult<Room>>;
};

export const createRoomsRepo = (pool: Pool): RoomsRepo => {
  return {
    list: async () => pool.query('SELECT * FROM rooms'),
    get: async (roomId: number) => {
      const result = await pool.query('Select * FROM rooms WHERE id=$1', [roomId]);
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return result.rows[0];
    },
    add: async (name: string, passwordHash: string, salt: string) => {
      const result = await pool.query(
        'INSERT INTO rooms(name, "passwordHash", salt) VALUES ($1, $2, $3) RETURNING *;',
        [name, passwordHash, salt],
      );
      // eslint-disable-next-line @typescript-eslint/no-unsafe-return
      return result.rows[0];
    },
  };
};
