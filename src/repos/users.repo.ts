import { Pool, QueryResult } from 'pg';
import { User } from '../types/user';

export type UsersRepo = {
  list: () => Promise<QueryResult<User[]>>;
  getUserById: (id: number) => Promise<User>;
  getUserByEmail: (email: string) => Promise<User>;
  createUser: (name: string, email: string, passwordHash: string, salt: string) => Promise<User>;
};

export const createUsersRepo = (pool: Pool): UsersRepo => {
  return {
    list: async () => pool.query('SELECT * FROM users'),
    getUserById: async id =>
      pool
        .query('SELECT * FROM users WHERE id = $1', [id])
        .then(result => {
          console.log('row:', result.rows);
          return result.rows[0] as User;
        })
        .catch((err: Error) => {
          console.log({ err });
          return (null as unknown) as User;
        }),
    getUserByEmail: async email =>
      pool
        .query('SELECT * FROM users WHERE email = $1', [email])
        .then(result => {
          return result.rows[0] as User;
        })
        .catch((err: Error) => {
          console.log({ err });
          return (null as unknown) as User;
        }),
    createUser: async (name, email, passwordHash, salt) => {
      return pool
        .query('INSERT INTO users (name, email, "passwordHash", salt) VALUES ($1, $2, $3, $4) RETURNING *', [
          name,
          email,
          passwordHash,
          salt,
        ])
        .then(result => {
          console.log('rows:', result.rows);
          return result.rows[0] as User;
        });
    },
  };
};
