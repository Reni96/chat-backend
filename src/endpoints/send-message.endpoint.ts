/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { FastifyInstance } from 'fastify';
import { isAuthenticated } from '../auth/is-authenticated-mw';
import { MessagesRepo } from '../repos/messages.repo';
import { MessageSchema } from '../schemas/message';
import { Message } from '../types/message';

type SendMessageEndpointFactory = (fastify: FastifyInstance, messagesRepo: MessagesRepo) => void;

export const sendMessageEndpointFactory: SendMessageEndpointFactory = (fastify, messagesRepo) => {
  fastify.route<{
    Body: { message: Message };
    Params: { roomId: number };
    Response: { status: Message };
  }>({
    method: 'POST',
    url: '/api/rooms/:roomId/messages',
    schema: {
      params: {
        roomiId: { type: 'string' },
      },
      body: {
        message: {
          data: { type: 'string' },
          createdAt: { type: 'string' },
          userId: { type: 'number' },
        },
      },
      response: {
        200: MessageSchema,
      },
    },
    preValidation: isAuthenticated,
    handler: async (request, response) => {
      const result = await messagesRepo.add(request.params.roomId, request.body.message);
      const { roomId } = request.params;

      // send message to all clients
      fastify.io.emit(`chat-${roomId}`, result);
      return response.status(200).send(result);
    },
  });
};
