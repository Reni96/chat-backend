import bcrypt from 'bcrypt';
import { FastifyInstance } from 'fastify';
import { isAuthenticated } from '../auth/is-authenticated-mw';
import { RoomsRepo } from '../repos/rooms.repo';
import { RoomSchema } from '../schemas/room';
import { Room } from '../types/room';

type CreateRoomEndpointFactory = (server: FastifyInstance, roomsRepo: RoomsRepo) => void;

export const createRoomEndpointFactory: CreateRoomEndpointFactory = (server, roomsRepo) => {
  server.route<{ Body: { name: string; password: string }; Response: { status: Room } }>({
    method: 'POST',
    url: '/api/rooms',
    schema: {
      body: {
        name: { type: 'string' },
        password: { type: 'string' },
      },
      response: {
        200: RoomSchema,
      },
    },
    preValidation: isAuthenticated,
    handler: async (request, response) => {
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(request.body.password, salt);

      const result = await roomsRepo.add(request.body.name, passwordHash, salt);
      return response.send(result);
    },
  });
};
