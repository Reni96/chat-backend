/* eslint-disable @typescript-eslint/no-unsafe-call */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
import { FastifyInstance } from 'fastify';
import passport from 'fastify-passport';
import { isAuthenticated } from '../auth/is-authenticated-mw';
import { UsersRepo } from '../repos/users.repo';
import { UserRegisterSchema } from '../schemas/user-register';
import { User } from '../types/user';
import { UserRegister } from '../types/user-register';

type LoginEndpointFactory = (server: FastifyInstance, usersRepo: UsersRepo) => void;

export const loginEndpointFactory: LoginEndpointFactory = (server, usersRepo) => {
  server.route<{ Body: { email: string; password: string }; Response: { status: UserRegister } }>({
    method: 'POST',
    url: '/api/login',
    schema: {
      body: {
        email: { type: 'string' },
        password: { type: 'string' },
      },
      response: {
        200: UserRegisterSchema,
      },
    },
    preHandler: passport.authenticate('local', { authInfo: false }),
    handler: async (request, response) => {
      return response.send({ email: request.body.email });
    },
  });

  server.route<{ Params: { id: string } }>({
    method: 'GET',
    url: '/api/me',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            id: { type: 'number' },
            name: { type: 'string' },
            email: { type: 'string' },
          },
        },
      },
    },
    preValidation: isAuthenticated,
    handler: async (request, _reply) => {
      console.log('valami', request.user);
      const { id } = request.user as User;
      // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
      const result = await usersRepo.getUserById(id);
      console.log(result);
      return _reply.status(200).send(result);
    },
  });

  server.post('/api/logout', (request, reply) => {
    request.session.delete();
    void reply.send({ status: 'Logged out' });
  });
};
