import { FastifyInstance } from 'fastify';
import { RoomsRepo } from '../repos/rooms.repo';
import { RoomSchema } from '../schemas/room';
import { Room } from '../types/room';

type GetRoomEndpointFactory = (fastify: FastifyInstance, roomsRepo: RoomsRepo) => void;

export const getRoomEndpointFactory: GetRoomEndpointFactory = (fastify, roomsRepo) => {
  fastify.route<{ Params: { roomId: number }; Response: { status: Room } }>({
    method: 'GET',
    url: '/api/rooms/:roomId',
    schema: {
      response: {
        200: RoomSchema,
      },
    },
    handler: async (request, response) => {
      const result = await roomsRepo.get(request.params.roomId);
      return response.send(result);
    },
  });
};
