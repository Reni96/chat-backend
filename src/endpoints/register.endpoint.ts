/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import bcrypt from 'bcrypt';
import { FastifyInstance } from 'fastify';
import { UsersRepo } from '../repos/users.repo';
import { UserRegisterSchema } from '../schemas/user-register';
import { UserRegister } from '../types/user-register';

type RegisterEndpointFactory = (server: FastifyInstance, userRepo: UsersRepo) => void;

export const registerEndpointFactory: RegisterEndpointFactory = (server, usersRepo) => {
  server.route<{ Body: { name: string; email: string; password: string }; Response: { status: UserRegister } }>({
    method: 'POST',
    url: '/api/signup',
    schema: {
      body: {
        name: { type: 'string' },
        email: { type: 'string' },
        password: { type: 'string' },
      },
      response: {
        200: UserRegisterSchema,
      },
    },
    handler: async (request, response) => {
      const salt = await bcrypt.genSalt(10);
      const passwordHash = await bcrypt.hash(request.body.password, salt);

      const result = await usersRepo.createUser(request.body.name, request.body.email, passwordHash, salt);
      return response.send(result);
    },
  });
};
