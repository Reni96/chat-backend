import { FastifyInstance } from 'fastify';
import { isAuthenticated } from '../auth/is-authenticated-mw';
import { MessagesRepo } from '../repos/messages.repo';
import { MessageSchema } from '../schemas/message';
import { Message } from '../types/message';

type GetMessagesEndpointFactory = (server: FastifyInstance, messagesRepo: MessagesRepo) => void;

export const getMessagesEndpointFactory: GetMessagesEndpointFactory = (server, messagesRepo) => {
  server.route<{ Params: { roomId: number }; Response: { items: Message } }>({
    method: 'GET',
    url: '/api/rooms/:roomId/messages',
    schema: {
      response: {
        200: {
          type: 'array',
          items: MessageSchema,
        },
      },
    },
    preValidation: isAuthenticated,
    handler: async (request, response) => {
      const result = (await messagesRepo.listByRoomId(request.params.roomId)).rows;
      return response.send(result);
    },
  });
};
