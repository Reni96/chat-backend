import { FastifyInstance } from 'fastify';
import { isAuthenticated } from '../auth/is-authenticated-mw';

type GetStatusEndpointFactory = (server: FastifyInstance) => void;

export const getStatusEnpointFactory: GetStatusEndpointFactory = server => {
  server.route({
    method: 'GET',
    url: '/api/status',
    schema: {
      response: {
        200: {
          type: 'object',
          properties: {
            status: { type: 'string' },
          },
        },
      },
    },
    preValidation: isAuthenticated,
    handler: async (_request, response) => {
      return response.send({ status: 'OK' });
    },
  });
};
