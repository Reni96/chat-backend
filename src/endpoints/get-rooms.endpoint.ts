import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { RoomsRepo } from '../repos/rooms.repo';
import { RoomSchema } from '../schemas/room';
import { Room } from '../types/room';

type GetRoomsEndpointFactory = (server: FastifyInstance, roomsRepo: RoomsRepo) => void;

export const getRoomsEndpointFactory: GetRoomsEndpointFactory = (server, roomsRepo) => {
  server.route<{ Body: void; Response: { items: Room } }>({
    method: 'GET',
    url: '/api/rooms',
    schema: {
      response: {
        200: {
          type: 'array',
          items: RoomSchema,
        },
      },
    },
    handler: async (_request: FastifyRequest, response: FastifyReply) => {
      const result = (await roomsRepo.list()).rows;
      return response.send(result);
    },
  });
};
