import { FastifyInstance } from 'fastify';
import { UsersRepo } from '../repos/users.repo';
import { UserSchema } from '../schemas/user';
import { User } from '../types/user';

type GetUserEndpointFactory = (fastify: FastifyInstance, usersRepo: UsersRepo) => void;

export const getUsersEndpointFactory: GetUserEndpointFactory = (fastify, usersRepo) => {
  fastify.route<{ Params: { id: number }; Response: { status: User } }>({
    method: 'GET',
    url: '/api/users',
    schema: {
      response: {
        200: {
          type: 'array',
          items: UserSchema,
        },
      },
    },
    handler: async (_request, response) => {
      const result = (await usersRepo.list()).rows;
      console.log('result:', result);
      return response.send(result);
    },
  });
};
